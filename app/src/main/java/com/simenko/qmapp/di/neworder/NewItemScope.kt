package com.simenko.qmapp.di.neworder

import javax.inject.Scope

@Scope
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class NewItemScope()
